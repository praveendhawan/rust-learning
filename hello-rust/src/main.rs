use lib_ruby_parser::{Parser, ParserOptions};
use std::fs::File;
use std::io::prelude::*;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let options = ParserOptions {
        buffer_name: "(eval)".to_owned(),
        debug: false,
        ..Default::default()
    };

    let mut file = File::open("/Users/praveen/workspace/Office/spree-jiffyshirts/app/models/spree/product_decorator.rb")?;
    let mut contents = String::new();
    file.read_to_string(&mut contents)?;

    let parser = Parser::new(contents.as_bytes(), options);

    println!("{:#?}", parser.do_parse());

    Ok(())
}
